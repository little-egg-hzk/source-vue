package cn.source.system.enums;

/**
 * 操作人类别
 *
 * @author luo
 */
public enum HouseStatus
    {
        AUDIT(0, "待审核") , SALEING(1, "待接单"), SALEED(2, "已接单"),
        CLOSE(3, "已下架"), FINISH(4,"装修完成"),PAID(5,"已支付"),RECEIPT(6,"已收款");
        private final Integer code;
        private final String info;

        HouseStatus(Integer code, String info)
        {
            this.code = code;
            this.info = info;
        }

        public Integer getCode()
        {
            return code;
        }

        public String getInfo()
        {
            return info;
        }
    }
