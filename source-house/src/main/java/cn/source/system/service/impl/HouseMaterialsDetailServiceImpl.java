package cn.source.system.service.impl;

import java.util.List;
import cn.source.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.source.system.mapper.HouseMaterialsDetailMapper;
import cn.source.system.domain.HouseMaterialsDetail;
import cn.source.system.service.IHouseMaterialsDetailService;

/**
 * 【装修材料中间表】Service业务层处理
 * 
 * @author sourcebyte.vip
 * @date 2023-11-26
 */
@Service
public class HouseMaterialsDetailServiceImpl implements IHouseMaterialsDetailService 
{
    @Autowired
    private HouseMaterialsDetailMapper houseMaterialsDetailMapper;

    /**
     * 查询【装修材料中间表】
     * 
     * @param id 【装修材料中间表】主键
     * @return 【装修材料中间表】
     */
    @Override
    public HouseMaterialsDetail selectHouseMaterialsDetailById(Long id)
    {
        return houseMaterialsDetailMapper.selectHouseMaterialsDetailById(id);
    }

    /**
     * 查询【装修材料中间表】列表
     * 
     * @param houseMaterialsDetail 【装修材料中间表】
     * @return 【装修材料中间表】
     */
    @Override
    public List<HouseMaterialsDetail> selectHouseMaterialsDetailList(HouseMaterialsDetail houseMaterialsDetail)
    {
        return houseMaterialsDetailMapper.selectHouseMaterialsDetailList(houseMaterialsDetail);
    }

    /**
     * 新增【装修材料中间表】
     * 
     * @param houseMaterialsDetail 【装修材料中间表】
     * @return 结果
     */
    @Override
    public int insertHouseMaterialsDetail(HouseMaterialsDetail houseMaterialsDetail)
    {
        houseMaterialsDetail.setCreateTime(DateUtils.getNowDate());
        return houseMaterialsDetailMapper.insertHouseMaterialsDetail(houseMaterialsDetail);
    }

    /**
     * 修改【装修材料中间表】
     * 
     * @param houseMaterialsDetail 【装修材料中间表】
     * @return 结果
     */
    @Override
    public int updateHouseMaterialsDetail(HouseMaterialsDetail houseMaterialsDetail)
    {
        houseMaterialsDetail.setUpdateTime(DateUtils.getNowDate());
        return houseMaterialsDetailMapper.updateHouseMaterialsDetail(houseMaterialsDetail);
    }

    /**
     * 批量删除【装修材料中间表】
     * 
     * @param ids 需要删除的【装修材料中间表】主键
     * @return 结果
     */
    @Override
    public int deleteHouseMaterialsDetailByIds(Long[] ids)
    {
        return houseMaterialsDetailMapper.deleteHouseMaterialsDetailByIds(ids);
    }

    /**
     * 删除【装修材料中间表】信息
     * 
     * @param id 【装修材料中间表】主键
     * @return 结果
     */
    @Override
    public int deleteHouseMaterialsDetailById(Long id)
    {
        return houseMaterialsDetailMapper.deleteHouseMaterialsDetailById(id);
    }
}
