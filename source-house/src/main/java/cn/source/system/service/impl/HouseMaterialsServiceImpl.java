package cn.source.system.service.impl;

import java.util.List;
import cn.source.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.source.system.mapper.HouseMaterialsMapper;
import cn.source.system.domain.HouseMaterials;
import cn.source.system.service.IHouseMaterialsService;

/**
 * 材料Service业务层处理
 * 
 * @author sourcebyte.vip
 * @date 2023-12-02
 */
@Service
public class HouseMaterialsServiceImpl implements IHouseMaterialsService 
{
    @Autowired
    private HouseMaterialsMapper houseMaterialsMapper;

    /**
     * 查询材料
     * 
     * @param id 材料主键
     * @return 材料
     */
    @Override
    public HouseMaterials selectHouseMaterialsById(Long id)
    {
        return houseMaterialsMapper.selectHouseMaterialsById(id);
    }

    /**
     * 查询材料列表
     * 
     * @param houseMaterials 材料
     * @return 材料
     */
    @Override
    public List<HouseMaterials> selectHouseMaterialsList(HouseMaterials houseMaterials)
    {
        return houseMaterialsMapper.selectHouseMaterialsList(houseMaterials);
    }

    /**
     * 新增材料
     * 
     * @param houseMaterials 材料
     * @return 结果
     */
    @Override
    public int insertHouseMaterials(HouseMaterials houseMaterials)
    {
        houseMaterials.setCreateTime(DateUtils.getNowDate());
        return houseMaterialsMapper.insertHouseMaterials(houseMaterials);
    }

    /**
     * 修改材料
     * 
     * @param houseMaterials 材料
     * @return 结果
     */
    @Override
    public int updateHouseMaterials(HouseMaterials houseMaterials)
    {
        return houseMaterialsMapper.updateHouseMaterials(houseMaterials);
    }

    /**
     * 批量删除材料
     * 
     * @param ids 需要删除的材料主键
     * @return 结果
     */
    @Override
    public int deleteHouseMaterialsByIds(Long[] ids)
    {
        return houseMaterialsMapper.deleteHouseMaterialsByIds(ids);
    }

    /**
     * 删除材料信息
     * 
     * @param id 材料主键
     * @return 结果
     */
    @Override
    public int deleteHouseMaterialsById(Long id)
    {
        return houseMaterialsMapper.deleteHouseMaterialsById(id);
    }
}
