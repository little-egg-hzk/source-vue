package cn.source.system.service;

import java.util.List;
import cn.source.system.domain.HouseMaterialsDetail;

/**
 * 【装修材料中间表】Service接口
 * 
 * @author sourcebyte.vip
 * @date 2023-11-26
 */
public interface IHouseMaterialsDetailService 
{
    /**
     * 查询【装修材料中间表】
     * 
     * @param id 【装修材料中间表】主键
     * @return 【装修材料中间表】
     */
    public HouseMaterialsDetail selectHouseMaterialsDetailById(Long id);

    /**
     * 查询【装修材料中间表】列表
     * 
     * @param houseMaterialsDetail 【装修材料中间表】
     * @return 【装修材料中间表】集合
     */
    public List<HouseMaterialsDetail> selectHouseMaterialsDetailList(HouseMaterialsDetail houseMaterialsDetail);

    /**
     * 新增【装修材料中间表】
     * 
     * @param houseMaterialsDetail 【装修材料中间表】
     * @return 结果
     */
    public int insertHouseMaterialsDetail(HouseMaterialsDetail houseMaterialsDetail);

    /**
     * 修改【装修材料中间表】
     * 
     * @param houseMaterialsDetail 【装修材料中间表】
     * @return 结果
     */
    public int updateHouseMaterialsDetail(HouseMaterialsDetail houseMaterialsDetail);

    /**
     * 批量删除【装修材料中间表】
     * 
     * @param ids 需要删除的【装修材料中间表】主键集合
     * @return 结果
     */
    public int deleteHouseMaterialsDetailByIds(Long[] ids);

    /**
     * 删除【装修材料中间表】信息
     * 
     * @param id 【装修材料中间表】主键
     * @return 结果
     */
    public int deleteHouseMaterialsDetailById(Long id);
}
