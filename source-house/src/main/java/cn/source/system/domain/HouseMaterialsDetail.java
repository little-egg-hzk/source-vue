package cn.source.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import cn.source.common.annotation.Excel;
import cn.source.common.core.domain.BaseEntity;

/**
 * 【装修材料中间表】对象 house_materials_detail
 * 
 * @author sourcebyte.vip
 * @date 2023-11-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HouseMaterialsDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private Long id;

    /** 材料id */
    @Excel(name = "材料id")
    private String materialsId;

    /** 房源id */
    @Excel(name = "房源id")
    private String houseId;

    /** 材料数量 */
    @Excel(name = "材料数量")
    private String materialsCount;

    /** 逻辑删除(0:存在1:删除) */
    @Excel(name = "逻辑删除(0:存在1:删除)")
    private Long deleted;

    @TableField(exist = false)
    private String materialsName;

    @TableField(exist = false)
    private String materialType;

}
