package cn.source.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.source.common.annotation.Log;
import cn.source.common.core.controller.BaseController;
import cn.source.common.core.domain.AjaxResult;
import cn.source.common.enums.BusinessType;
import cn.source.system.domain.HouseMaterialsDetail;
import cn.source.system.service.IHouseMaterialsDetailService;
import cn.source.common.utils.poi.ExcelUtil;
import cn.source.common.core.page.TableDataInfo;

/**
 * 【装修材料中间表】Controller
 * 
 * @author sourcebyte.vip
 * @date 2023-11-26
 */
@RestController
@RequestMapping("/system/detail")
public class HouseMaterialsDetailController extends BaseController
{
    @Autowired
    private IHouseMaterialsDetailService houseMaterialsDetailService;

    /**
     * 查询【装修材料中间表】列表
     */
    @PreAuthorize("@ss.hasPermi('system:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(HouseMaterialsDetail houseMaterialsDetail)
    {
        startPage();
        List<HouseMaterialsDetail> list = houseMaterialsDetailService.selectHouseMaterialsDetailList(houseMaterialsDetail);
        return getDataTable(list);
    }

    /**
     * 导出【装修材料中间表】列表
     */
    @PreAuthorize("@ss.hasPermi('system:detail:export')")
    @Log(title = "【装修材料中间表】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, HouseMaterialsDetail houseMaterialsDetail)
    {
        List<HouseMaterialsDetail> list = houseMaterialsDetailService.selectHouseMaterialsDetailList(houseMaterialsDetail);
        ExcelUtil<HouseMaterialsDetail> util = new ExcelUtil<HouseMaterialsDetail>(HouseMaterialsDetail.class);
        util.exportExcel(response, list, "【装修材料中间表】数据");
    }

    /**
     * 获取【装修材料中间表】详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:detail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(houseMaterialsDetailService.selectHouseMaterialsDetailById(id));
    }

    /**
     * 新增【装修材料中间表】
     */
    @PreAuthorize("@ss.hasPermi('system:detail:add')")
    @Log(title = "【装修材料中间表】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HouseMaterialsDetail houseMaterialsDetail)
    {
        return toAjax(houseMaterialsDetailService.insertHouseMaterialsDetail(houseMaterialsDetail));
    }

    /**
     * 修改【装修材料中间表】
     */
    @PreAuthorize("@ss.hasPermi('system:detail:edit')")
    @Log(title = "【装修材料中间表】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HouseMaterialsDetail houseMaterialsDetail)
    {
        return toAjax(houseMaterialsDetailService.updateHouseMaterialsDetail(houseMaterialsDetail));
    }

    /**
     * 删除【装修材料中间表】
     */
    @PreAuthorize("@ss.hasPermi('system:detail:remove')")
    @Log(title = "【装修材料中间表】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(houseMaterialsDetailService.deleteHouseMaterialsDetailByIds(ids));
    }
}
