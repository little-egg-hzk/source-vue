package cn.source.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.source.common.annotation.Log;
import cn.source.common.core.controller.BaseController;
import cn.source.common.core.domain.AjaxResult;
import cn.source.common.enums.BusinessType;
import cn.source.system.domain.HouseMaterials;
import cn.source.system.service.IHouseMaterialsService;
import cn.source.common.utils.poi.ExcelUtil;
import cn.source.common.core.page.TableDataInfo;

/**
 * 材料Controller
 * 
 * @author sourcebyte.vip
 * @date 2023-12-02
 */
@RestController
@RequestMapping("/system/materials")
public class HouseMaterialsController extends BaseController
{
    @Autowired
    private IHouseMaterialsService houseMaterialsService;

    /**
     * 查询材料列表
     */
    @PreAuthorize("@ss.hasPermi('system:materials:list')")
    @GetMapping("/list")
    public TableDataInfo list(HouseMaterials houseMaterials)
    {
        startPage();
        List<HouseMaterials> list = houseMaterialsService.selectHouseMaterialsList(houseMaterials);
        return getDataTable(list);
    }

    /**
     * 导出材料列表
     */
    @PreAuthorize("@ss.hasPermi('system:materials:export')")
    @Log(title = "材料", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, HouseMaterials houseMaterials)
    {
        List<HouseMaterials> list = houseMaterialsService.selectHouseMaterialsList(houseMaterials);
        ExcelUtil<HouseMaterials> util = new ExcelUtil<HouseMaterials>(HouseMaterials.class);
        util.exportExcel(response, list, "材料数据");
    }

    /**
     * 获取材料详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:materials:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(houseMaterialsService.selectHouseMaterialsById(id));
    }

    /**
     * 新增材料
     */
    @PreAuthorize("@ss.hasPermi('system:materials:add')")
    @Log(title = "材料", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HouseMaterials houseMaterials)
    {
        return toAjax(houseMaterialsService.insertHouseMaterials(houseMaterials));
    }

    /**
     * 修改材料
     */
    @PreAuthorize("@ss.hasPermi('system:materials:edit')")
    @Log(title = "材料", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HouseMaterials houseMaterials)
    {
        return toAjax(houseMaterialsService.updateHouseMaterials(houseMaterials));
    }

    /**
     * 删除材料
     */
    @PreAuthorize("@ss.hasPermi('system:materials:remove')")
    @Log(title = "材料", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(houseMaterialsService.deleteHouseMaterialsByIds(ids));
    }
}
