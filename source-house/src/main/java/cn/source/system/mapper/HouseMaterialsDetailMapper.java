package cn.source.system.mapper;

import java.util.List;
import cn.source.system.domain.HouseMaterialsDetail;

/**
 * 【装修材料中间表】Mapper接口
 * 
 * @author sourcebyte.vip
 * @date 2023-11-26
 */
public interface HouseMaterialsDetailMapper 
{
    /**
     * 查询【装修材料中间表】
     * 
     * @param id 【装修材料中间表】主键
     * @return 【装修材料中间表】
     */
    public HouseMaterialsDetail selectHouseMaterialsDetailById(Long id);

    /**
     * 查询【装修材料中间表】列表
     * 
     * @param houseMaterialsDetail 【装修材料中间表】
     * @return 【装修材料中间表】集合
     */
    public List<HouseMaterialsDetail> selectHouseMaterialsDetailList(HouseMaterialsDetail houseMaterialsDetail);

    /**
     * 新增【装修材料中间表】
     * 
     * @param houseMaterialsDetail 【装修材料中间表】
     * @return 结果
     */
    public int insertHouseMaterialsDetail(HouseMaterialsDetail houseMaterialsDetail);

    /**
     * 修改【装修材料中间表】
     * 
     * @param houseMaterialsDetail 【装修材料中间表】
     * @return 结果
     */
    public int updateHouseMaterialsDetail(HouseMaterialsDetail houseMaterialsDetail);

    /**
     * 删除【装修材料中间表】
     * 
     * @param id 【装修材料中间表】主键
     * @return 结果
     */
    public int deleteHouseMaterialsDetailById(Long id);

    /**
     * 批量删除【装修材料中间表】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHouseMaterialsDetailByIds(Long[] ids);
}
