package cn.source.system.mapper;

import java.util.List;
import cn.source.system.domain.HouseMaterials;

/**
 * 材料Mapper接口
 * 
 * @author sourcebyte.vip
 * @date 2023-12-02
 */
public interface HouseMaterialsMapper 
{
    /**
     * 查询材料
     * 
     * @param id 材料主键
     * @return 材料
     */
    public HouseMaterials selectHouseMaterialsById(Long id);

    /**
     * 查询材料列表
     * 
     * @param houseMaterials 材料
     * @return 材料集合
     */
    public List<HouseMaterials> selectHouseMaterialsList(HouseMaterials houseMaterials);

    /**
     * 新增材料
     * 
     * @param houseMaterials 材料
     * @return 结果
     */
    public int insertHouseMaterials(HouseMaterials houseMaterials);

    /**
     * 修改材料
     * 
     * @param houseMaterials 材料
     * @return 结果
     */
    public int updateHouseMaterials(HouseMaterials houseMaterials);

    /**
     * 删除材料
     * 
     * @param id 材料主键
     * @return 结果
     */
    public int deleteHouseMaterialsById(Long id);

    /**
     * 批量删除材料
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHouseMaterialsByIds(Long[] ids);
}
